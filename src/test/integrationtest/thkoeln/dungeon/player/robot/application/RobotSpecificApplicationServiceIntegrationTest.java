package thkoeln.dungeon.player.robot.application;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import thkoeln.dungeon.player.core.domainprimitives.purchasing.Money;
import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotRevealedDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotRevealedLevelDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotInventoryDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotInventoryResourcesDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotPlanetDto;
import thkoeln.dungeon.player.player.application.PlayerApplicationService;
import thkoeln.dungeon.player.player.domain.Player;
import thkoeln.dungeon.player.player.domain.PlayerRepository;
import thkoeln.dungeon.player.robot.domain.Robot;
import thkoeln.dungeon.player.robot.domain.RobotRepository;

import java.util.UUID;


@DirtiesContext
@SpringBootTest
public class RobotSpecificApplicationServiceIntegrationTest {
    @Autowired
    private RobotRepository robotRepository;
    @Autowired
    private PlayerRepository playerRepository;
    @Autowired
    private PlayerApplicationService playerApplicationService;
    @Autowired
    private RobotSpecificApplicationService robotApplicationService;

    @BeforeEach
    public void setUp() throws Exception {
        robotRepository.deleteAll();
        playerRepository.deleteAll();
    }

    @Test
    public void test_newRobot_bothDtos() throws Exception {
//        given
        RobotPlanetDto robotPlanetDto = new RobotPlanetDto();
        robotPlanetDto.setPlanetId(UUID.randomUUID());
        robotPlanetDto.setGameWorldId(UUID.randomUUID());
        robotPlanetDto.setResourceType("COAL");
        robotPlanetDto.setMovementDifficulty(1);

        RobotInventoryDto robotInventoryDto = new RobotInventoryDto();
        robotInventoryDto.setMaxStorage(10);
        robotInventoryDto.setResources(new RobotInventoryResourcesDto());

        UUID playerId = UUID.randomUUID();

        RobotRevealedLevelDto revealedLevelDto = RobotRevealedLevelDto.defaults();
//        when
        RobotDto correctRobotDto = new RobotDto();
        RobotDto wrongRobotDto = new RobotDto();
        RobotRevealedDto correctRevealedDto = new RobotRevealedDto();
        RobotRevealedDto wrongRevealedDto = new RobotRevealedDto();

        correctRobotDto.setPlayer(playerId);
        correctRobotDto.setPlanet(robotPlanetDto);
        correctRobotDto.setId(UUID.randomUUID());
        correctRobotDto.setInventory(robotInventoryDto);
        correctRobotDto.setHealth(10);
        correctRobotDto.setEnergy(10);
        correctRobotDto.setMiningSpeed(1);
        correctRobotDto.setMaxHealth(10);
        correctRobotDto.setMaxEnergy(10);
        correctRobotDto.setEnergyRegen(10);
        correctRobotDto.setAttackDamage(10);

        correctRevealedDto.setRobotId(UUID.randomUUID());
        correctRevealedDto.setPlanetId(UUID.randomUUID());
        correctRevealedDto.setPlayerNotion(playerId.toString());
        correctRevealedDto.setHealth(10);
        correctRevealedDto.setEnergy(10);
        correctRevealedDto.setLevels(revealedLevelDto);

        wrongRevealedDto.setHealth(10);
        wrongRevealedDto.setEnergy(10);
        wrongRevealedDto.setLevels(revealedLevelDto);
//        then
        Robot newRobot = robotApplicationService.newRobot(correctRobotDto);


        //        correctRobotDto
        Assertions.assertNotNull(newRobot);
        Assertions.assertTrue(robotRepository.existsById(correctRobotDto.getId()));
        robotRepository.delete(correctRobotDto.getId());

        //        correctRobotRevealedDto
        robotApplicationService.newRobot(correctRevealedDto);
        Assertions.assertTrue(robotRepository.existsById(correctRevealedDto.getRobotId()));

        //        wrongRobotDto
        wrongRobotDto.setPlayer(null);
        Robot wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setPlayer(playerId);

        wrongRobotDto.setPlanet(new RobotPlanetDto());
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setPlanet(robotPlanetDto);

        wrongRobotDto.setId(null);
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setId(UUID.randomUUID());

        wrongRobotDto.setInventory(new RobotInventoryDto());
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setInventory(robotInventoryDto);

        wrongRobotDto.setHealth(0);
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setHealth(10);

        wrongRobotDto.setEnergy(0);
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setEnergy(10);

        wrongRobotDto.setMiningSpeed(0);
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setMiningSpeed(1);

        wrongRobotDto.setMaxHealth(0);
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setMaxHealth(10);

        wrongRobotDto.setMaxEnergy(0);
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setMaxEnergy(10);

        wrongRobotDto.setEnergyRegen(0);
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setEnergyRegen(10);

        wrongRobotDto.setAttackDamage(0);
        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNull(wrongRobot);
        Assertions.assertFalse(robotRepository.existsById(wrongRobotDto.getId()));
        wrongRobotDto.setAttackDamage(10);

        wrongRobot = robotApplicationService.newRobot(wrongRobotDto);
        Assertions.assertNotNull(wrongRobot);
        Assertions.assertTrue(robotRepository.existsById(wrongRobotDto.getId()));
        robotRepository.delete(wrongRobot.getId());
        //        wrongRobotRevealedDto
        wrongRevealedDto.setRobotId(null);
        robotApplicationService.newRobot(wrongRevealedDto);
        Assertions.assertFalse(robotRepository.existsById(wrongRevealedDto.getRobotId()));
        wrongRevealedDto.setRobotId(UUID.randomUUID());

        wrongRevealedDto.setPlanetId(null);
        robotApplicationService.newRobot(wrongRevealedDto);
        Assertions.assertFalse(robotRepository.existsById(wrongRevealedDto.getRobotId()));
        wrongRevealedDto.setPlanetId(UUID.randomUUID());

        wrongRevealedDto.setPlayerNotion(null);
        robotApplicationService.newRobot(wrongRevealedDto);
        Assertions.assertFalse(robotRepository.existsById(wrongRevealedDto.getRobotId()));
        wrongRevealedDto.setPlayerNotion(playerId.toString());

        robotApplicationService.newRobot(wrongRevealedDto);
        Assertions.assertTrue(robotRepository.existsById(wrongRevealedDto.getRobotId()));
    }

    /**
    * something something
    * mock something something
    **/
    @Test
    public void test_robotActions() {
//        given
        int counter = 0;
        Player player = playerApplicationService.queryAndIfNeededCreatePlayer();
        player.setBankAccount(Money.from(1300));

        while (counter < 5) {
            RobotPlanetDto robotPlanetDto = new RobotPlanetDto();
            robotPlanetDto.setPlanetId(UUID.randomUUID());
            robotPlanetDto.setGameWorldId(UUID.randomUUID());
            robotPlanetDto.setResourceType("COAL");
            robotPlanetDto.setMovementDifficulty(1);

            RobotInventoryDto robotInventoryDto = new RobotInventoryDto();
            robotInventoryDto.setMaxStorage(10);
            robotInventoryDto.setResources(new RobotInventoryResourcesDto());

            RobotDto robotDto = new RobotDto();

            robotDto.setPlayer(player.getPlayerId());
            robotDto.setPlanet(robotPlanetDto);
            robotDto.setId(UUID.randomUUID());
            robotDto.setInventory(robotInventoryDto);
            robotDto.setHealth(10);
            robotDto.setEnergy(10);
            robotDto.setMiningSpeed(1);
            robotDto.setMaxHealth(10);
            robotDto.setMaxEnergy(10);
            robotDto.setEnergyRegen(10);
            robotDto.setAttackDamage(10);

            Robot robot = robotApplicationService.newRobot(robotDto);
            robotRepository.saveMyRobot(robot);
            counter++;
        }
    }
}
