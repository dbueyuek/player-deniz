package thkoeln.dungeon.player.robot.application;

import jakarta.transaction.Transactional;
import org.jetbrains.annotations.Nullable;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import thkoeln.dungeon.player.MovementException;
import thkoeln.dungeon.player.core.domainprimitives.command.Command;
import thkoeln.dungeon.player.core.domainprimitives.location.CompassDirection;
import thkoeln.dungeon.player.core.domainprimitives.location.MineableResource;
import thkoeln.dungeon.player.core.domainprimitives.location.MineableResourceType;
import thkoeln.dungeon.player.core.domainprimitives.purchasing.Capability;
import thkoeln.dungeon.player.core.domainprimitives.purchasing.CapabilityType;
import thkoeln.dungeon.player.core.domainprimitives.purchasing.Money;
import thkoeln.dungeon.player.core.events.concreteevents.game.RoundStatusEvent;
import thkoeln.dungeon.player.core.events.concreteevents.game.RoundStatusType;
import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotRevealedDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotDto;
import thkoeln.dungeon.player.core.restadapter.GameServiceRESTAdapter;
import thkoeln.dungeon.player.map.domain.Map;
import thkoeln.dungeon.player.player.application.PlayerApplicationService;
import thkoeln.dungeon.player.player.domain.Player;
import thkoeln.dungeon.player.robot.domain.*;

import java.util.Objects;
import java.util.UUID;

@Service
public class RobotSpecificApplicationService {

    private final Logger logger = LoggerFactory.getLogger(PlayerApplicationService.class);
    private final RobotRepository robotRepository;
    private final PlayerApplicationService playerApplicationService;
    private final RobotSpecificPlanetApplicationService robotSpecificPlanetApplicationService;
    private final GameServiceRESTAdapter gameServiceRESTAdapter;

    public RobotSpecificApplicationService(
            RobotRepository robotRepository,
            PlayerApplicationService playerApplicationService,
            RobotSpecificPlanetApplicationService robotSpecificPlanetApplicationService,
            GameServiceRESTAdapter gameServiceRESTAdapter
    ) {
        this.robotRepository = robotRepository;
        this.playerApplicationService = playerApplicationService;
        this.robotSpecificPlanetApplicationService = robotSpecificPlanetApplicationService;
        this.gameServiceRESTAdapter = gameServiceRESTAdapter;
    }

    /**
     * Decides every action a robot takes depending on money, roundNumber and role
     * @param event A RobotStatusEvent, containing the roundStatus and roundNumber
     */
    public void robotActions( RoundStatusEvent event ) {
        if ( !event.getRoundStatus().equals(RoundStatusType.STARTED) ) return;
        if ( robotRepository.findMyRobots().isEmpty() ) return;

        Player player = playerApplicationService.queryAndIfNeededCreatePlayer();
        int availableMoney = player.getBankAccount().getAmount();
        if (availableMoney >= 200) availableMoney -= 100;
        int explorerCount = 0, fighterCount = 0, robotNumber = 0;
        int myRobotCount = robotRepository.findMyRobots().size();

        logger.info( "You have {} Robots", robotRepository.findMyRobots().size() );
        for ( Robot robot: robotRepository.findMyRobots() ) {
            robotNumber++;
            logger.info("{}. Robot: {} is on Planet: {} and has {} Energy left ({})",
                    robotNumber, robot.getId(), robot.getPlanet().getPlanetId(), robot.getEnergy(), robot.getRole());
            if (robot.getRole() == Role.EXPLORER) explorerCount++;
            if (robot.getRole() == Role.FIGHTER) fighterCount++;
        }

        for ( Robot robot: robotRepository.findMyRobots() ) {
            if ( myRobotCount % 15 == 0 && explorerCount != ( myRobotCount / 15 ) + 1 || explorerCount == 0 )
                if ( robot.equals( robotRepository.findMineByLowestLevel() ) )
                    explorerCount = setExplorer(robot, explorerCount);
            if ( myRobotCount % 10 == 0 && fighterCount < ( myRobotCount / 10 ) )
                if ( robot.equals( robotRepository.findMineByLowestLevel() ) )
                    fighterCount = setFighter(robot, fighterCount);

            if (robot.getRunAwayCounter() > 0) {
                runAway(robot, player);
                continue;
            }
            if ( robot.getRole() != Role.MINER && robot.getExplorerIdleCounter() <= 3 ) {
                if ( robot.getInventory().getUsedStorage() > 0 )
                    sellResources(robot, player, event.getRoundNumber());
                else
                    availableMoney = upgradeOrExplore(robot, player, availableMoney);
                continue;
            }
            if (robot.getRole() != Role.MINER)
                robot.setExplorerIdleCounter(robot.getExplorerIdleCounter() - 1);
            if (robot.getRole() == Role.MINER && robot.getNextMove() == null) setPath(robot);

            if ( sellResources(robot, player, event.getRoundNumber()) ) continue;

            if (robot.getEnergy() <= 5) {
                regenerate(robot, player);
                continue;
            }

            availableMoney = upgradeFollowPathOrExplore(robot, player, availableMoney);
        }
    }

    /**
     * Creates a new robot from a given RobotDto
     * @param robotDto A RobotDto, containing all the parameters of the robot to be created
     * @return a new Robot, made up of the given parameters
     */
    @Transactional
    public Robot newRobot(RobotDto robotDto) {
        if ( !robotDto.isValid() ) return null;

        Robot newRobot = new Robot( robotDto );
        newRobot.setPlanet( robotSpecificPlanetApplicationService.getPlanetFromDto(robotDto.getPlanet()) );
        robotRepository.save( newRobot );
        return newRobot;
    }

    /**
     * Creates a new robot from a given RobotRevealedDto
     * @param robotDto A RobotRevealedDto, containing all the parameters of the robot to be created
     */
    public void newRobot(RobotRevealedDto robotDto) {
        if ( !robotDto.isValid() ) return;

        Robot newRobot = new Robot( robotDto );
        newRobot.setPlanet( robotSpecificPlanetApplicationService.getPlanetFromUUID( robotDto.getPlanetId() ) );
        save( newRobot );
    }

    /**
     * Moves a robot that is currently running away, randomly across the map or
     * regenerates energy if the robot can't move anymore
     * @param robot
     * @param player
     */
    private void runAway( Robot robot, Player player ) {
        robot.setRunAwayCounter(robot.getRunAwayCounter() - 1);
        try {
            move(robot, player);
        } catch (MovementException movementException) {
            regenerate(robot, player);
        }
    }

    /**
     * Either upgrades the robot or makes it explore the map, by going to unknown planets,
     * depending on the available money of the player and their ability to upgrade the robot
     * @param robot
     * @param player
     * @param availableMoney
     * @return
     */
    private Integer upgradeOrExplore(Robot robot, Player player, Integer availableMoney) {
        int newAvailableMoney;
        newAvailableMoney = upgradeRobot(robot, player, availableMoney);
        if (newAvailableMoney == availableMoney)
            exploreMap(robot, player);
        return newAvailableMoney;
    }

    /**
     * Either upgrades the robot or makes it explore the map, by going to unknown planets or following a set path,
     * depending on the available money of the player and their ability to upgrade the robot
     * @param robot
     * @param player
     * @param availableMoney
     * @return
     */
    private Integer upgradeFollowPathOrExplore(Robot robot, Player player, Integer availableMoney) {
        int newAvailableMoney;
        newAvailableMoney = upgradeRobot(robot, player, availableMoney);
        if (newAvailableMoney == availableMoney) {
            if (robot.getNextMove() != null)
                moveAlongPath(robot, player);
            else
                mineOrExplore(robot, player);
        }
        return newAvailableMoney;
    }

    /**
     * Turns the robot into an explorer, whose main goal it is to explore the map
     * @param robot
     * @param explorerCount The amount of existing explorers for the player
     * @return
     */
    private Integer setExplorer(Robot robot, Integer explorerCount) {
        logger.info( "Robot: {} is feeling extra curious tonight", robot.getId() );
        robot.setRole(Role.EXPLORER);
        save(robot);
        return ++explorerCount;
    }

    /**
     * Turns the robot into a fighter, whose main goal it is to fight other robots
     * @param robot
     * @param fighterCount The amount of existing fighters for the player
     * @return
     */
    private Integer setFighter(Robot robot, Integer fighterCount) {
        logger.info( "Robot: {} is feeling extra aggressive tonight", robot.getId() );
        robot.setRole(Role.FIGHTER);
        save(robot);
        return ++fighterCount;
    }

    /**
     * Sets a path to the closest planet with the highest rated mineral the robot can mine at the moment.
     * @param robot
     */
    private void setPath(Robot robot) {
        MineableResourceType type = getMineableResourceType(robot);

        if (robot.getPlanet().getResource() != null && type != null &&
                robot.getPlanet().getResource().getResourceType() == type) {
            return;
        }
        if (type != null) {
            robot.setPathToMineral(Map.findPathToMineralType(
                    robotSpecificPlanetApplicationService.getPlanets(),
                    robot.getPlanet().getPlanetId(),
                    type));
            save(robot);
        }
        if (!robot.getPathToMineral().isEmpty() || robot.getNextMove() != null)
            logger.info( "Robot: {} found a path to find {}. path: {} nextMove: {}",
                    robot.getId(), type, robot.getPathToMineral(), robot.getNextMove() );
    }

    /**
     * Return the highest rated minerale a robot can mine
     * @param robot
     * @return The MineableResourceType for a robot, depending on the robots miningLevel
     */
    private static @Nullable MineableResourceType getMineableResourceType(Robot robot) {
        MineableResourceType type = null;

        if (robot.getMiningLevel() == 4) {
            type = MineableResourceType.PLATIN;
        } else if (robot.getMiningLevel() == 3) {
            type = MineableResourceType.GOLD;
        } else if (robot.getMiningLevel() == 2) {
            type = MineableResourceType.GEM;
        } else if (robot.getMiningLevel() == 1) {
            type = MineableResourceType.IRON;
        } else if (robot.getMiningLevel() == 0) {
            type = MineableResourceType.COAL;
        }
        return type;
    }

    /**
     * Upgrades a robot depending on the available money and their role
     * @param robot
     * @param player
     * @param availableMoney
     * @return The availableMoney, after expenditures
     */
    private Integer upgradeRobot( Robot robot, Player player, Integer availableMoney ) {
        if ( availableMoney != null )
            switch (robot.getRole()) {
                case EXPLORER, FIGHTER -> { if ( availableMoney < 300 ) return availableMoney; }
                case MINER -> { if ( availableMoney < 100 ) return availableMoney; }
            }

        int expenditure = 0;

        if (Money.from(availableMoney).canBuyThatManyFor( Money.from(4000) ) >= 1) {
            expenditure = upgradeLevel( robot, player, expenditure, 4 );
        } else if (Money.from(availableMoney).canBuyThatManyFor( Money.from(1500) ) >= 1) {
            expenditure = upgradeLevel( robot, player, expenditure, 3 );
        } else if ( Money.from(availableMoney).canBuyThatManyFor( Money.from(300) ) >= 1) {
            expenditure = upgradeLevel( robot, player, expenditure, 2 );
        } else if (Money.from(availableMoney).canBuyThatManyFor( Money.from(50) ) >= 1) {
            expenditure = upgradeLevel( robot, player, expenditure, 1 );
        }

        availableMoney -= expenditure;
        return availableMoney;
    }

    /**
     * Upgrades an explorer robots most important levels
     * @param robot
     * @param player
     * @param expenditure
     * @param level
     * @return The cost of the purchased upgrade
     */
    private Integer explorerUpgrades(Robot robot, Player player, Integer expenditure, Integer level) {
        if (robot.getLevelByType(CapabilityType.ENERGY_REGEN) < level)
            expenditure = upgradeByType(robot, player, CapabilityType.ENERGY_REGEN);
        else if (robot.getLevelByType(CapabilityType.MAX_ENERGY) < level)
            expenditure = upgradeByType(robot, player, CapabilityType.MAX_ENERGY);
        return expenditure;
    }

    /**
     * Upgrades a robots essentials, that being either the storage or their energy
     * @param robot
     * @param player
     * @param expenditure
     * @param level
     * @return The cost of the purchased upgrade
     */
    private Integer essentialUpgrades(Robot robot, Player player, Integer expenditure, Integer level) {
        if (robot.getLevelByType(CapabilityType.STORAGE) < level) {
            expenditure = upgradeByType(robot, player, CapabilityType.STORAGE);
        } else expenditure = explorerUpgrades(robot, player, expenditure, level);
        return expenditure;
    }

    /**
     * Upgrades a robot if their level is below the upgradeable level, depending on available money and role
     * @param robot
     * @param player
     * @param expenditure
     * @param level
     * @return The cost of the purchased upgrades
     */
    private Integer upgradeLevel( Robot robot, Player player, Integer expenditure, Integer level ) {
        switch ( robot.getRole() ) {
            case EXPLORER -> expenditure = explorerUpgrades(robot, player, expenditure, level);
            case MINER -> {
                if (robot.getLevelByType(CapabilityType.MINING_SPEED) < level) {
                    expenditure = upgradeByType(robot, player, CapabilityType.MINING_SPEED);
                } else if (robot.getLevelByType(CapabilityType.MINING) < level) {
                    expenditure = upgradeByType(robot, player, CapabilityType.MINING);
                } else expenditure = essentialUpgrades( robot, player, expenditure, level );
            }
            case FIGHTER -> {
                if (robot.getLevelByType(CapabilityType.DAMAGE) < level) {
                    expenditure = upgradeByType(robot, player, CapabilityType.DAMAGE);
                } else expenditure = essentialUpgrades( robot, player, expenditure, level );
            }
        }
        return expenditure;
    }

    /**
     * Send a post request for a specific upgrade
     * @param robot
     * @param player
     * @param type The type of upgrade to be made
     * @return The cost of the purchased upgrade
     */
    private Integer upgradeByType( Robot robot, Player player, CapabilityType type ) {
        int[] upgradeCosts = {50, 300, 1500, 4000, 15000};

        Capability capability = Capability.forTypeAndLevel(type, robot.getLevelByType(type)).nextLevel();
        Command command = Command.createUpgrade(
                1,
                capability,
                robot.getId(),
                player.getGameId(),
                player.getPlayerId());
        gameServiceRESTAdapter.sendPostRequestForCommand( command );
        logger.info( "Robot: {} is attempting to upgrade to: {}",
                robot.getId(), Capability.forTypeAndLevel(type, robot.getLevelByType(type)).nextLevel());

        return upgradeCosts[capability.getLevel() - 1];
    }

    /**
     * Sells the whole inventory of a robot, depending on robots inventory, the roundNumber and the robots recent upgrade status
     * @param robot
     * @param player
     * @param roundNumber
     * @return A boolean that says wether a sale has been made
     */
    private boolean sellResources( Robot robot, Player player, Integer roundNumber ) {
        if ( robot.getInventory().getFilled() || robot.getRecentlyUpgraded() || roundNumber % 5 == 0 )
            for ( MineableResourceType type: MineableResourceType.values() ) {
                if ( sellAllOfType(robot, player, type) ) {
                    if ( Objects.equals( robot.getInventory().getUsedStorage(), robot.getInventory().getResources().getFromType(type) ) )
                        robot.setRecentlyUpgraded(false);
                    save( robot );
                    return true;
                }
            }
        return false;
    }

    /**
     * Sells all occurences of a type of mineral, in a specific robots inventory
     * @param robot
     * @param player
     * @param type
     * @return A boolean that says wether a type of mineral is still available in the inventory of a robot
     */
    private boolean sellAllOfType( Robot robot, Player player, MineableResourceType type ) {
        if ( robot.getInventory().getResources().getFromType(type) <= 0 ) return false;
        Command command = Command.createSelling(
                robot.getId(),
                player.getGameId(),
                player.getPlayerId(),
                MineableResource.fromTypeAndAmount(type, robot.getInventory().getResources().getFromType(type))
        );
        gameServiceRESTAdapter.sendPostRequestForCommand( command );
        logger.info( "Robot: {} is attempting to sell: {} {}",
                robot.getId(), robot.getInventory().getResources().getFromType(type), type );
        return true;
    }

    /**
     * Makes a robot rest, to regain his energy
     * @param robot
     * @param player
     */
    private void regenerate( Robot robot, Player player ) {
        Command command = Command.createRegeneration(robot.getId(), player.getGameId(), player.getPlayerId());
        gameServiceRESTAdapter.sendPostRequestForCommand( command );
        logger.info("Robot: {} is regenerating", robot.getId());
    }

    /**
     * Moves the robot, so that he visits unknown planets.
     * If all neighbouring planets are known, the robot moves in a random direction and their idleCounter is incremented.
     * If the robot can't move, he rests and restores his energy.
     * @param robot
     * @param player
     */
    private void exploreMap(Robot robot, Player player) {
        try {
            UUID targetPlanet = Map.exploreMap(
                    robotSpecificPlanetApplicationService.getPlanets(),
                    robot.getPlanet(),
                    robot.getEnergy());

            Command command = Command.createMove(
                    robot.getId(),
                    targetPlanet,
                    player.getGameId(),
                    player.getPlayerId());

            gameServiceRESTAdapter.sendPostRequestForCommand( command );
            logger.info( "Robot: {} is exploring map", robot.getId() );
        } catch ( MovementException movementException ) {
            if (movementException.getMessage().equals( "All neighbours are known!" )) {
                try {
                    move(robot, player);
                    if ( robot.getRole() == Role.EXPLORER || robot.getRole() == Role.FIGHTER ) robot.increaseExplorerIdleCounter();
                    save(robot);
                } catch (MovementException randomMovementException) {
                    regenerate(robot, player);
                }
            } else {
                regenerate(robot, player);
            }
        }
    }

    /**
     * Moves a robot along a set path
     * @param robot
     * @param player
     */
    private void moveAlongPath(Robot robot, Player player) {
        if ( robot.getPlanet().getNeighbours().isEmpty() ) return;
        if ( robot.getEnergy() < robot.getPlanet().getMovementDifficulty() )
            regenerate(robot, player);

        UUID nextMoveId = robot.getNextMove();
        CompassDirection neighbourDirection = null;
        Integer movementDifficulty = robotSpecificPlanetApplicationService.getPlanetFromUUID(
                nextMoveId
        ).getMovementDifficulty();

        if ( movementDifficulty != null && robot.getEnergy() < movementDifficulty )
            regenerate(robot, player);

        for ( CompassDirection direction: robot.getPlanet().getNeighbours().keySet() ) {
            if (robot.getPlanet().getNeighbours().get(direction).equals(nextMoveId)) {
                neighbourDirection = direction;
                break;
            }
        }

        Command command = Command.createMove(
                robot.getId(),
                nextMoveId,
                player.getGameId(),
                player.getPlayerId()
        );
        gameServiceRESTAdapter.sendPostRequestForCommand( command );
        logger.info( "Robot: {} is moving along a Path. Next headed {} to Planet: {}",
                robot.getId(), neighbourDirection, nextMoveId);
        robot.updateNextMove();
        save(robot);
    }

    /**
     * Moves a robot randomly, but not to the previously visited planet.
     * @param robot
     * @param player
     * @throws MovementException An Exception, that's thrown if a robot doesn't have the sufficient energy to move
     */
    private void move(Robot robot, Player player) throws MovementException {
        if ( robot.getPlanet().getNeighbours().isEmpty() ) return;
        if ( robot.getEnergy() < robot.getPlanet().getMovementDifficulty() )
            throw new MovementException( "You can't move without enough energy!" );

        CompassDirection neighbourDirection = robot.getPlanet().getNeighbours().keySet().iterator().next();
        UUID neighbourPlanetId = robot.getPlanet().getNeighbours().get(neighbourDirection);
        logger.info( "Robot: {} can move to {}", robot.getId(), robot.getPlanet().getNeighbours() );

        for ( CompassDirection direction: robot.getPlanet().getNeighbours().keySet() ) {
            if ( !robot.getPlanet().getNeighbours().get(direction).equals( robot.getPreviousPlanet() ) ) {
                neighbourDirection = direction;
                neighbourPlanetId = robot.getPlanet().getNeighbours().get(neighbourDirection);
                break;
            }
        }

        Command command = Command.createMove(
                robot.getId(),
                neighbourPlanetId,
                player.getGameId(),
                player.getPlayerId()
        );
        gameServiceRESTAdapter.sendPostRequestForCommand( command );
        logger.info( "Robot: {} is attempting to move {} to Planet: {}",
                robot.getId(), neighbourDirection, neighbourPlanetId );
    }

    /**
     * Makes a robot mine on a planet or move to an unknown planet, based on their miningLevel
     * @param robot
     * @param player
     */
    private void mineOrExplore( Robot robot, Player player ) {
        if ( robot.getPlanet().getResource() == null || robot.getPlanet().getResource().getCurrentAmount() <= 0) {
            exploreMap(robot, player);
            return;
        }

        if (robot.getMiningLevel() == 4) {
            if (robot.getPlanet().getResource().getResourceType() != MineableResourceType.PLATIN) {
                exploreMap(robot, player);
                return;
            }
        } else if (robot.getMiningLevel() == 3) {
            if (robot.getPlanet().getResource().getResourceType() != MineableResourceType.GOLD) {
                exploreMap(robot, player);
                return;
            }
        } else if (robot.getMiningLevel() == 2) {
            if (robot.getPlanet().getResource().getResourceType() != MineableResourceType.GEM) {
                exploreMap(robot, player);
                return;
            }
        } else if (robot.getMiningLevel() == 1) {
            if (robot.getPlanet().getResource().getResourceType() != MineableResourceType.IRON) {
                exploreMap(robot, player);
                return;
            }
        } else if (robot.getMiningLevel() == 0) {
            if (robot.getPlanet().getResource().getResourceType() != MineableResourceType.COAL) {
                exploreMap(robot, player);
                return;
            }
        }

        Command command = Command.createMining(
                robot.getInventory().getMaxStorage() - robot.getInventory().getUsedStorage(),
                robot.getId(),
                robot.getPlanet().getPlanetId(),
                player.getGameId(),
                player.getPlayerId()
        );

        gameServiceRESTAdapter.sendPostRequestForCommand(command);
        logger.info("Robot: {} is attempting to mine: {}",
                robot.getId(), robot.getPlanet().getResource().getResourceType().toString());
    }

    /**
     * Makes the personal robot fight or run away from another robot, depending on their role
     * @param myRobot
     * @param otherRobot
     */
    public void fightOrFlightRobot(Robot myRobot, Robot otherRobot) {
        if ( myRobot.getRole() != Role.FIGHTER )
            myRobot.setRunAwayCounter(myRobot.getRunAwayCounter() + 1);
        else {
            Player player = playerApplicationService.queryAndIfNeededCreatePlayer();
            Command command;
            if ( myRobot.getHealth() > 10 && getHealthDifference( myRobot, otherRobot ) > -2 &&
                myRobot.getDamageLevel() >= otherRobot.getDamageLevel()
            ) {
                command = Command.createFight(
                        myRobot.getId(),
                        player.getGameId(),
                        player.getPlayerId(),
                        otherRobot.getId());
                gameServiceRESTAdapter.sendPostRequestForCommand( command );
                logger.info( "Your Robot: {} is attempting to attack Robot {}", myRobot.getId(), otherRobot.getId() );
                logger.info( "Your Robot Health: {} DamageLevel: {}, Other Robot Health: {} DamageLevel: {}",
                        myRobot.getHealth(), myRobot.getDamageLevel(), otherRobot.getHealth(), otherRobot.getDamageLevel() );
            }
            else
                myRobot.setRunAwayCounter(myRobot.getRunAwayCounter() + 1);
        }
    }

    /**
     * Return the difference in health between two robots
     * @param myRobot
     * @param otherRobot
     * @return The difference in health, between one robot and another
     */
    private Integer getHealthDifference( Robot myRobot, Robot otherRobot ) {
        return myRobot.getHealth() - otherRobot.getHealth();
    }

    /**
     * Saves a robot or changes to a robot in a general and also personal list, depending on the robots affiliation to the player
     * @param robot
     */
    public void save( Robot robot ) {
        robotRepository.save( robot );
        if (robotRepository.oneOfMine(robot))
            robotRepository.saveMyRobot(robot);
    }
}