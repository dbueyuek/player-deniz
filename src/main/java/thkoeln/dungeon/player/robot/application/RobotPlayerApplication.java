package thkoeln.dungeon.player.robot.application;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import thkoeln.dungeon.player.player.application.PlayerSpecificRobotApplication;
import thkoeln.dungeon.player.robot.domain.RobotRepository;

@Service
public class RobotPlayerApplication implements PlayerSpecificRobotApplication {

    private final RobotRepository robotRepository;

    @Autowired
    public RobotPlayerApplication(RobotRepository robotRepository) {
        this.robotRepository = robotRepository;
    }

    /**
     * Returns the number of robots for the own player
     * @return The number of robots for the own player
     */
    @Override
    public Integer getNumberOfRobotsForPlayer() {
        return robotRepository.findMyRobots().size();
    }
}
