package thkoeln.dungeon.player.robot.application;

import jakarta.transaction.Transactional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import thkoeln.dungeon.player.core.events.concreteevents.game.GameStatusEvent;
import thkoeln.dungeon.player.core.events.concreteevents.game.RoundStatusEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.change.RobotRegeneratedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.change.RobotRestoredAttributesEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.change.RobotUpgradedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.fight.RobotAttackedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.mine.RobotResourceMinedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.mine.RobotResourceRemovedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.move.RobotMovedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotRevealedDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotsRevealedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotSpawnedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.trading.TradableSoldEvent;
import thkoeln.dungeon.player.player.application.PlayerApplicationService;
import thkoeln.dungeon.player.player.domain.Player;
import thkoeln.dungeon.player.robot.domain.*;

import java.util.ArrayList;

@Service
public class RobotEventListenerService {
    private final Logger logger = LoggerFactory.getLogger(PlayerApplicationService.class);
    private final RobotRepository robotRepository;
    private final PlayerApplicationService playerApplicationService;
    private final RobotSpecificPlanetApplicationService robotSpecificPlanetApplicationService;
    private final RobotSpecificApplicationService robotApplicationService;

    @Autowired
    public RobotEventListenerService(
            RobotRepository robotRepository,
            PlayerApplicationService playerApplicationService,
            RobotSpecificPlanetApplicationService robotSpecificPlanetApplicationService,
            RobotSpecificApplicationService robotApplicationService
    ) {
        this.robotRepository = robotRepository;
        this.playerApplicationService = playerApplicationService;
        this.robotSpecificPlanetApplicationService = robotSpecificPlanetApplicationService;
        this.robotApplicationService = robotApplicationService;
    }

    /**
     * Starts all the actions of all robots depending on roundStatus and roundNumber
     * @param event A RoundStatusEvent, containing the roundStatus and roundNumber
     */
    @EventListener( RoundStatusEvent.class )
    public void robotActions( RoundStatusEvent event ) {
        robotApplicationService.robotActions( event );
    }

    /**
     * Updates and saves the inventory of a specific robot
     * @param event A RobotResourceMinedEvent, containing the updated inventory of a specific robot
     */
    @EventListener(RobotResourceMinedEvent.class)
    public void onResourceMined( RobotResourceMinedEvent event ) {
        if ( !event.isValid() ) return;
        if ( !robotRepository.existsById(event.getRobotId()) ) return;

        Robot robot = robotRepository.findById(event.getRobotId());
        robot.setInventory( new RobotInventory( robot, new RobotInventoryResources( event.getResourceInventory() ) ) );
        robotApplicationService.save( robot );
        logger.info( "Robot: {} mined {} {}", robot.getId(), event.getMinedAmount(), event.getMinedResource() );
        logger.info( "Updated Robot Inventory: +{} {}, Storage: {}",
                event.getMinedAmount(), event.getMinedResource(), robot.getInventory() );
    }

    /**
     * Updates the current and previous planet for a specific robot, as well as its energy
     * @param event A RobotMovedEvent, containing previous and new planet of a specific robot
     */
    @EventListener( RobotMovedEvent.class )
    public void robotMoved( RobotMovedEvent event ) {
        if ( !event.isValid() ) return;
        if ( !robotRepository.existsById(event.getRobotId()) ) return;

        Robot robot = robotRepository.findById(event.getRobotId());
        robot.setPlanet(robotSpecificPlanetApplicationService.getPlanetFromMoveDto(robot.getPlanet(), event.getToPlanet()));
        robot.setEnergy( event.getRemainingEnergy() );
        robot.setPreviousPlanet( event.getFromPlanet().getId() );
        robotApplicationService.save( robot );
        logger.info( "Robot: {} moved From Planet: {} to Planet: {}",
                robot.getId(), event.getFromPlanet(), event.getToPlanet() );
    }

    /**
     * Updates the inventory of a specific robot
     * @param event A RobotResourceRemovedEvent, containing the updated inventory of a specific robot
     */
    @EventListener(RobotResourceRemovedEvent.class)
    public void robotResourceRemoved( RobotResourceRemovedEvent event ) {
        if ( !event.isValid() ) return;
        if ( !robotRepository.existsById(event.getRobotId()) ) return;

        Robot robot = robotRepository.findById(event.getRobotId());
        robot.setInventory( new RobotInventory( robot, new RobotInventoryResources( event.getResourceInventory() ) ) );
        robotApplicationService.save( robot );
        logger.info( "Robot: {} removed {} {}", robot.getId(), event.getRemovedAmount(), event.getRemovedResource() );
    }

    /**
     * Creates and saves a new robot in a general and personal list
     * @param event A RobotSpawnedEvent, containing all details of a newly spawned robot
     */
    @EventListener( RobotSpawnedEvent.class )
    @Transactional
    public void robotSpawned( RobotSpawnedEvent event ) {
        Player player = playerApplicationService.queryAndIfNeededCreatePlayer();
        if ( !event.getRobotDto().getPlayer().equals( player.getPlayerId() ) ) return;
        if ( !event.getRobotDto().isValid() ) {
            logger.info("Robot: {} is not valid", event.getRobotDto().getId());
            return;
        }
        logger.info( "Newly spawned Robot is being registered" );
        Robot robot = robotApplicationService.newRobot( event.getRobotDto() );
        logger.info( "New Robot: {}", robot );
        logger.info( "On Planet: {}", robot.getPlanet().toString() );
        robotRepository.saveMyRobot(robot);
        logger.info("New Robot: {} added to Player {}", robot.getId(), player.getPlayerId() );
    }

    /**
     * (1) Updates the list of robots, personal and general.
     * (2) Makes personal robots fight or run away, depending on the other robots stats
     * @param event A RobotsRevealedEvent, containing a list of robots on planets that are occupied by the player
     */
    @EventListener( RobotsRevealedEvent.class )
    public void robotsRevealed( RobotsRevealedEvent event ) {
        if ( !event.isValid() ) return;

        Robot robot;

        for ( RobotRevealedDto robotDto: event.getRobots() ) {
            if ( !robotRepository.existsById(robotDto.getRobotId()) ) {
                robotApplicationService.newRobot(robotDto);
            }
            else {
                robot = robotRepository.findById(robotDto.getRobotId());
                robot.setPlanet(robotSpecificPlanetApplicationService.getPlanetFromUUID(robotDto.getPlanetId()));
                robot.setHealth(robot.getHealth());
                robot.setEnergy(robot.getEnergy());
                robot.setLevels(robotDto.getLevels());
                robotApplicationService.save(robot);
            }
            if ( !robotRepository.oneOfMine(robotDto.getRobotId()) ) {
                for (Robot myRobot : robotRepository.findMyRobots()) {
                    if ( myRobot.getPlanet().getPlanetId().equals( robotDto.getPlanetId() ) ) {
                        robotApplicationService.fightOrFlightRobot(myRobot, myRobot);
                    }
                }
            }
        }
    }

    /**
     * Updates the levels for a specific robot
     * @param event A RobotUpgradedEvent, containing all the info of a newly upgraded robot
     */
    @EventListener( RobotUpgradedEvent.class )
    public void robotUpgraded( RobotUpgradedEvent event ) {
        if ( !event.isValid() ) return;
        if ( !robotRepository.existsById(event.getRobotId()) ) return;

        Robot robot = robotRepository.findById( event.getRobotId() );
        robot.setLevelsFromDto(event.getRobotDto());
        if ( event.getUpgrade().equals( "MINING_SPEED" ) )
            robot.setRecentlyUpgraded(true);
        robotApplicationService.save( robot );
        logger.info( "Robot: {} {}-{} upgraded to {}-{}",
                robot.getId(),
                event.getUpgrade(), event.getLevel() - 1,
                event.getUpgrade(), event.getLevel());

    }

    /**
     * updates the available energy for a specific robot
     * @param event A RobotRegeneratedEvent, containing the available Energy for a specific robot
     */
    @EventListener( RobotRegeneratedEvent.class )
    public void robotRegenerated( RobotRegeneratedEvent event ) {
        if ( !event.isValid() ) return;
        if ( !robotRepository.existsById(event.getRobotId()) ) return;

        Robot robot = robotRepository.findById(event.getRobotId());
        int oldEnergy = robot.getEnergy();
        robot.setEnergy(event.getAvailableEnergy());
        logger.info( "Robot: {} regenerated {} Energy. Available energy: {}",
                robot.getId(), robot.getEnergy() - oldEnergy, event.getAvailableEnergy() );
    }

    /**
     * Deletes all saved robots after a game has ended
     * @param event A GameStatusEvent, containing all infos for a game
     */
    @EventListener( GameStatusEvent.class )
    public void gameStatusEvent( GameStatusEvent event ) {
        if ( !event.isValid() ) return;
        if ( !event.getStatus().isActive() )
            robotRepository.deleteAll();
    }

    /**
     * Informs you about the sale done by a robot
     * @param event A TradableSoldEvent, containing all infos regarding a sale
     */
    @EventListener( TradableSoldEvent.class )
    public void tradableSoldEvent( TradableSoldEvent event ) {
        if ( !event.isValid() ) return;
        if ( !robotRepository.existsById(event.getRobotId()) ) return;

        logger.info( "Robot: {} sold {} {} for {} money",
                event.getRobotId(), event.getName(), event.getAmount(), event.getTotalPrice() );
    }

    /**
     * (1) Updates the robots with the parameters of the fight
     * (2) Makes personal robots run away from a fight
     * @param event A RobotAttackedEvent, containing all the infos of a robot fight
     */
    @EventListener( RobotAttackedEvent.class )
    public void robotAttackedEvent( RobotAttackedEvent event ) {
        if ( !event.isValid() ) return;

        Robot robotTarget;

        if ( robotRepository.oneOfMine( event.getTarget().getRobotId() ) ) {
            robotTarget = robotRepository.findById(event.getTarget().getRobotId());
            robotTarget.setHealth(event.getTarget().getAvailableHealth());
            robotTarget.setEnergy(event.getTarget().getAvailableEnergy());
            robotTarget.setAlive(event.getTarget().getAlive());
            if ( !event.getTarget().getAlive() ) {
                logger.info( "Robot: {} got destroyed!", event.getTarget().getRobotId() );
                robotRepository.delete(robotTarget);
                return;
            }
            robotTarget.setPathToMineral( new ArrayList<>() );
            robotTarget.setRunAwayCounter(3);
            robotApplicationService.save( robotTarget );
            logger.info( "Robot: {} is attempting to run away from Robot: {}",
                    robotTarget.getId(), event.getAttacker().getRobotId() );
        }
    }

    /**
     * Updates a specific robots Health or Energy depending on the restoration item used
     * @param event A RobotRestoredAttributesEvent, containing a robots basic attributed and the restoration item
     *              used to restore them
     */
    @EventListener( RobotRestoredAttributesEvent.class )
    public void robotRestoredAttributesEvent( RobotRestoredAttributesEvent event ) {
        if ( !event.isValid() ) return;
        if ( !robotRepository.existsById(event.getRobotId()) ) return;

        Robot robot = robotRepository.findById(event.getRobotId());
        robot.setHealth( robot.getHealth() );
        robot.setEnergy( robot.getEnergy() );
        String loggerInfo = "Robot: " + robot.getId() + " has restored his " + event.getRestorationType() + ". New Value: ";
        if (event.getRestorationType().equals("HEALTH"))
            loggerInfo = loggerInfo + robot.getHealth();
        else
            loggerInfo = loggerInfo + robot.getEnergy();

        robotApplicationService.save( robot );
        logger.info( loggerInfo );
    }
}
