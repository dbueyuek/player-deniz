package thkoeln.dungeon.player.robot.application;

import thkoeln.dungeon.player.core.events.concreteevents.robot.move.RobotMovePlanetDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotPlanetDto;
import thkoeln.dungeon.player.planet.domain.Planet;

import java.util.HashMap;
import java.util.UUID;

public interface RobotSpecificPlanetApplicationService {
    /**
     * Returns a planet created with the information of a RobotPlanetDto
     * @param planetDto
     * @return A Planet, created with the information of a RobotPlanetDto
     */
    Planet getPlanetFromDto(RobotPlanetDto planetDto);

    /**
     * Returns a planet created with the information of a RobotMovePlanetDto
     * @param oldPlanet
     * @param moveDto
     * @return A Planet, created with the information of a RobotMovePlanetDto
     */
    Planet getPlanetFromMoveDto(Planet oldPlanet, RobotMovePlanetDto moveDto);

    /**
     * Returns a planet, by the associated UUID
     * @param planetID
     * @return A new or known planet, depending on existence in known planet repository
     */
    Planet getPlanetFromUUID(UUID planetID);

    /**
     * Returns all currently known planets
     * @return All known planets
     */
    HashMap<UUID, Planet> getPlanets();
}

