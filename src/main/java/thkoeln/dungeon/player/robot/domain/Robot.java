package thkoeln.dungeon.player.robot.domain;

import jakarta.persistence.*;
import lombok.*;
import thkoeln.dungeon.player.core.domainprimitives.purchasing.CapabilityType;
import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotRevealedDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.reveal.RobotRevealedLevelDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotDto;
import thkoeln.dungeon.player.planet.domain.Planet;
import thkoeln.dungeon.player.player.domain.AbstractRobot;

import java.util.LinkedList;
import java.util.List;
import java.util.UUID;

import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;
import static thkoeln.dungeon.player.core.domainprimitives.purchasing.Capability.MIN_LEVEL;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class Robot extends AbstractRobot {
    @OneToOne ( fetch = FetchType.EAGER, cascade = CascadeType.ALL )
    private Planet planet;
    private UUID previousPlanet;
    private Boolean alive = TRUE;
    @Embedded
    private RobotInventory inventory;
    private Integer health;
    private Integer energy;
    private Integer healthLevel = MIN_LEVEL;
    private Integer damageLevel = MIN_LEVEL;
    private Integer miningSpeedLevel = MIN_LEVEL;
    private Integer miningLevel = MIN_LEVEL;
    private Integer energyLevel = MIN_LEVEL;
    private Integer energyRegenLevel = MIN_LEVEL;
    private Integer miningSpeed;
    private Integer maxHealth;
    private Integer maxEnergy;
    private Integer energyRegen;
    private Integer attackDamage;
    @ElementCollection
    private LinkedList<UUID> pathToMineral;
    private UUID nextMove;
    private Role role = Role.MINER;
    private Integer explorerIdleCounter = 0;
    private Boolean recentlyUpgraded = FALSE;
    private Integer runAwayCounter = 0;

    public String toString() {
        return "Robot{" +
                "player=" + getPlayer() +
                ", id=" + getId() +
                ", alive=" + alive +
                ", inventory=" + inventory.toString() +
                ", health=" + health +
                ", energy=" + energy +
                ", healthLevel=" + healthLevel +
                ", damageLevel=" + damageLevel +
                ", miningSpeedLevel=" + miningSpeedLevel +
                ", miningLevel=" + miningLevel +
                ", energyLevel=" + energyLevel +
                ", energyRegenLevel=" + energyRegenLevel +
                ", miningSpeed=" + miningSpeed +
                ", maxHealth=" + maxHealth +
                ", maxEnergy=" + maxEnergy +
                ", energyRegen=" + energyRegen +
                ", attackDamage=" + attackDamage +
                '}';
    }

    public Robot(RobotDto robotDto) {
        super(robotDto.getPlayer(), robotDto.getId());
        this.alive = robotDto.getAlive();
        this.inventory = new RobotInventory( robotDto );
        this.health = robotDto.getHealth();
        this.energy = robotDto.getEnergy();
        this.healthLevel = robotDto.getHealthLevel();
        this.damageLevel = robotDto.getDamageLevel();
        this.miningSpeedLevel = robotDto.getMiningSpeedLevel();
        this.energyLevel = robotDto.getEnergyLevel();
        this.miningLevel = robotDto.getMiningLevel();
        this.energyRegenLevel = robotDto.getEnergyRegenLevel();
        this.miningSpeed = robotDto.getMiningSpeed();
        this.maxHealth = robotDto.getMaxHealth();
        this.maxEnergy = robotDto.getMaxEnergy();
        this.energyRegen = robotDto.getEnergyRegen();
        this.attackDamage = robotDto.getAttackDamage();
    }

    public Robot(RobotRevealedDto robotDto) {
        super(UUID.fromString(robotDto.getPlayerNotion()), robotDto.getRobotId());
        this.alive = TRUE;
        this.inventory = new RobotInventory(robotDto.getLevels().getStorageLevel(), new RobotInventoryResources());
        this.health = robotDto.getHealth();
        this.energy = robotDto.getEnergy();
        this.healthLevel = robotDto.getLevels().getHealthLevel();
        this.damageLevel = robotDto.getLevels().getDamageLevel();
        this.miningSpeedLevel = robotDto.getLevels().getMiningSpeedLevel();
        this.energyLevel = robotDto.getLevels().getEnergyLevel();
        this.miningLevel = robotDto.getLevels().getMiningLevel();
        this.energyRegenLevel = robotDto.getLevels().getEnergyRegenLevel();
        this.miningSpeed = 1;
        this.maxHealth = robotDto.getHealth();
        this.maxEnergy = robotDto.getEnergy();
        this.energyRegen = 1;
        this.attackDamage = 1;
    }

    public void setPathToMineral(List<UUID> path) {
        this.pathToMineral = new LinkedList<>(path);
        if (!pathToMineral.isEmpty())
            this.nextMove = pathToMineral.poll();
        else
            this.nextMove = null;
    }

    public void updateNextMove() {
        if (!pathToMineral.isEmpty())
            this.nextMove = pathToMineral.poll();
        else
            this.nextMove = null;
    }

    public void setLevels(RobotRevealedLevelDto levels) {
        this.healthLevel = levels.getHealthLevel();
        this.damageLevel = levels.getDamageLevel();
        this.miningSpeedLevel = levels.getMiningSpeedLevel();
        this.miningLevel = levels.getMiningLevel();
        this.energyLevel = levels.getEnergyLevel();
        this.energyRegenLevel = levels.getEnergyRegenLevel();
        this.inventory = new RobotInventory(this.inventory, levels.getStorageLevel());
    }

    public void setLevelsFromDto( RobotDto dto ) {
        this.healthLevel = dto.getHealthLevel();
        this.damageLevel = dto.getDamageLevel();
        this.miningSpeedLevel = dto.getMiningSpeedLevel();
        this.miningLevel = dto.getMiningLevel();
        this.energyLevel = dto.getEnergyLevel();
        this.energyRegenLevel = dto.getEnergyRegenLevel();
        this.miningSpeed = dto.getMiningSpeed();
        this.maxHealth = dto.getMaxHealth();
        this.maxEnergy = dto.getMaxEnergy();
        this.energyRegen  = dto.getEnergyRegen();
        this.attackDamage = dto.getDamageLevel();
        this.inventory = new RobotInventory(dto.getInventory());
    }

    public Integer getLevelByType(CapabilityType type) {
        if (type == CapabilityType.DAMAGE)
            return damageLevel;
        else if (type == CapabilityType.ENERGY_REGEN)
            return energyRegenLevel;
        else if (type.equals(CapabilityType.HEALTH))
            return healthLevel;
        else if (type.equals(CapabilityType.MAX_ENERGY))
            return energyLevel;
        else if (type.equals(CapabilityType.MINING))
            return miningLevel;
        else if (type.equals(CapabilityType.MINING_SPEED))
            return miningSpeedLevel;
        else if (type.equals(CapabilityType.STORAGE))
            return inventory.getStorageLevel();
        else return null;
    }

    public void increaseExplorerIdleCounter() {
        this.explorerIdleCounter += 1;
    }

}
