package thkoeln.dungeon.player.robot.domain;

public enum Role {
    EXPLORER,
    FIGHTER,
    MINER
}
