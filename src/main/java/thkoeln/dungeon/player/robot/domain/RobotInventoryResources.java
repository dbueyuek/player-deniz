package thkoeln.dungeon.player.robot.domain;


import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.ToString;
import thkoeln.dungeon.player.core.domainprimitives.location.MineableResourceType;
import thkoeln.dungeon.player.core.events.concreteevents.robot.mine.RobotResourceInventoryDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotInventoryResourcesDto;

@ToString
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class RobotInventoryResources {
    private Integer coal = 0;
    private Integer iron = 0;
    private Integer gem = 0;
    private Integer gold = 0;
    private Integer platin = 0;

    public Integer getFromType(MineableResourceType type) {
        if (type == MineableResourceType.COAL) {
            return coal;
        } else if (type == MineableResourceType.IRON) {
            return iron;
        } else if (type == MineableResourceType.GEM) {
            return gem;
        } else if (type == MineableResourceType.GOLD) {
            return gold;
        } else if (type == MineableResourceType.PLATIN) {
            return platin;
        } else return null;
    }

    public RobotInventoryResources ( RobotInventoryResourcesDto robotInventoryResourcesDto ) {
        this.coal = robotInventoryResourcesDto.getCoal();
        this.iron = robotInventoryResourcesDto.getIron();
        this.gem = robotInventoryResourcesDto.getGem();
        this.gold = robotInventoryResourcesDto.getGold();
        this.platin = robotInventoryResourcesDto.getPlatin();
    }
    public RobotInventoryResources ( RobotResourceInventoryDto robotResourceInventoryDto ) {
        this.coal = robotResourceInventoryDto.getCoal();
        this.iron = robotResourceInventoryDto.getIron();
        this.gem = robotResourceInventoryDto.getGem();
        this.gold = robotResourceInventoryDto.getGold();
        this.platin = robotResourceInventoryDto.getPlatin();
    }

    public Integer getTotalAmount() {
        return coal + iron + gem + gold + platin;
    }
}

