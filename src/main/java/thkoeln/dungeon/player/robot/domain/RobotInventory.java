package thkoeln.dungeon.player.robot.domain;

import jakarta.persistence.Embeddable;
import jakarta.persistence.Embedded;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotInventoryDto;

@Getter
@ToString
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class RobotInventory {
    private Integer storageLevel = 0;
    private Integer usedStorage = 0;
    @Embedded
    private RobotInventoryResources resources;
    private Boolean filled = Boolean.FALSE;
    private Integer maxStorage;

    public RobotInventory( RobotDto robotDto ) {
        this.storageLevel = robotDto.getInventory().getStorageLevel();
        this.usedStorage = robotDto.getInventory().getUsedStorage();
        this.resources = new RobotInventoryResources( robotDto.getInventory().getResources() );
        this.filled = robotDto.getInventory().getFull();
        this.maxStorage = robotDto.getInventory().getMaxStorage();
    }

    public RobotInventory( Robot robot, RobotInventoryResources resources ) {
        this.storageLevel = robot.getInventory().getStorageLevel();
        this.usedStorage = resources.getTotalAmount();
        this.resources = resources;
        this.maxStorage = robot.getInventory().getMaxStorage();
        if ( usedStorage.equals(maxStorage) )
            this.filled = Boolean.TRUE;
        else
            this.filled = Boolean.FALSE;
    }

    public RobotInventory( Integer storageLevel, RobotInventoryResources resources ) {
        this.storageLevel = storageLevel;
        this.usedStorage = resources.getTotalAmount();
        this.resources = resources;
        this.maxStorage = 0;
        if ( usedStorage.equals(maxStorage) )
            this.filled = Boolean.TRUE;
        else
            this.filled = Boolean.FALSE;
    }

    public RobotInventory( RobotInventory inventory, Integer storageLevel ) {
        this.storageLevel = storageLevel;
        this.usedStorage = inventory.getUsedStorage();
        this.resources = inventory.getResources();
        this.filled = inventory.getFilled();
        this.maxStorage = inventory.getMaxStorage();
    }

    public RobotInventory(RobotInventoryDto dto) {
        this.storageLevel = dto.getStorageLevel();
        this.usedStorage = dto.getUsedStorage();
        this.resources = new RobotInventoryResources( dto.getResources() );
        this.filled = dto.getFull();
        this.maxStorage = dto.getMaxStorage();
    }
}
