package thkoeln.dungeon.player.robot.domain;

import org.springframework.stereotype.Repository;
import thkoeln.dungeon.player.core.domainprimitives.purchasing.CapabilityType;

import java.util.*;

@Repository
public class RobotRepository {
    private final HashMap<UUID, Robot> robots = new HashMap<>();
    private final HashMap<UUID, Robot> myRobots = new HashMap<>();

    public Robot findById(UUID uuid) {
        return robots.get(uuid);
    }

    public Robot findMineByLowestLevel() {
        Robot lowestLevelRobot = new Robot();
        int currentTotalLevel = 0, previousTotalLevel = 0, firstCounter = 0;

        for (Robot robot : myRobots.values()) {
            if (robot.getRole() != Role.MINER) continue;
            if (firstCounter == 0) {
                lowestLevelRobot = robot;
                firstCounter++;
            }
            for (CapabilityType capabilityType: CapabilityType.values()) {
                currentTotalLevel += robot.getLevelByType(capabilityType);
            }
            if (currentTotalLevel < previousTotalLevel) {
                lowestLevelRobot = robot;
            }
            previousTotalLevel = currentTotalLevel;
        }

        return lowestLevelRobot;
    }

    public List<Robot> findAll() {
        return new ArrayList<>(robots.values());
    }

    public boolean existsById(UUID uuid) {
        return robots.containsKey(uuid);
    }

    public List<Robot> findAllById(Set<UUID> uuids) {
        List<Robot> result = new ArrayList<>();
        for (UUID uuid : uuids) {
            result.add(findById(uuid));
        }
        return result;
    }

    public void save(Robot robot) {
        robots.put(robot.getId(), robot);
    }

    public boolean oneOfMine(Robot robot) {
        return myRobots.containsKey(robot.getId());
    }

    public List<Robot> findMyRobots() {
        return new ArrayList<>(myRobots.values());
    }

    public boolean oneOfMine(UUID uuid) {
        return myRobots.containsKey(uuid);
    }

    public void saveMyRobot(Robot robot) {
        myRobots.put(robot.getId(), robot);
    }

    public void deleteAll() {
        robots.clear();
        myRobots.clear();
    }

    public void delete(UUID uuid) {
        robots.remove(uuid);
        myRobots.remove(uuid);
    }

    public void delete(Robot robot) {
        robots.remove(robot.getId());
        myRobots.remove(robot.getId());
    }
}
