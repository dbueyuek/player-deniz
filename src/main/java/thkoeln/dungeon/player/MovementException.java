package thkoeln.dungeon.player;

public class MovementException extends Exception {
    public MovementException(String errorMessage) {super(errorMessage);}
}
