package thkoeln.dungeon.player.player.application;

public interface PlayerSpecificRobotApplication {
    Integer getNumberOfRobotsForPlayer();
}
