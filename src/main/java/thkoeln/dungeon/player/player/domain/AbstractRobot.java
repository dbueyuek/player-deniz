package thkoeln.dungeon.player.player.domain;

import jakarta.persistence.*;
import lombok.*;

import java.util.UUID;

@Entity
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractRobot {
    private UUID player;
//    private RobotPlanetDto planet;
    @Id
    private UUID id;

//    private RobotInventoryDto inventory;
}
