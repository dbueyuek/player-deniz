package thkoeln.dungeon.player.player.domain;

import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class PlayerRepository {
    private final Map<UUID, Player> players = new HashMap<>();

    public List<Player> findAll() {
        return new ArrayList<>(players.values());
    }

    public void save(Player player) {
        players.put(player.getPlayerId(), player);
    }

    public void deleteAll() {
        players.clear();
    }
}
