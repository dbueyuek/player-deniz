package thkoeln.dungeon.player.core.domainprimitives.location;

import com.fasterxml.jackson.annotation.JsonValue;

/**
 * Enum to represent different types of mineable resources
 */
public enum MineableResourceType {
    COAL,
    IRON,
    GEM,
    GOLD,
    PLATIN;
}
