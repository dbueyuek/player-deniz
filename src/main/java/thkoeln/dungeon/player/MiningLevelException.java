package thkoeln.dungeon.player;

public class MiningLevelException extends Exception {
    public MiningLevelException(String errorMessage) {super(errorMessage);}
}
