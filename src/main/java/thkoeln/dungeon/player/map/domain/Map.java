package thkoeln.dungeon.player.map.domain;

import thkoeln.dungeon.player.MovementException;
import thkoeln.dungeon.player.core.domainprimitives.location.CompassDirection;
import thkoeln.dungeon.player.core.domainprimitives.location.MineableResourceType;
import thkoeln.dungeon.player.planet.domain.Planet;

import java.util.*;

public class Map {

    /**
     * Returns a list containing all the UUIDs of planets, that form a path to a planet with a specified resource type
     * @param planets
     * @param startId
     * @param type
     * @return List of planets UUIDs
     */
    public static List<UUID> findPathToMineralType( HashMap<UUID, Planet> planets, UUID startId, MineableResourceType type) {
        Queue<UUID> queue = new LinkedList<>();
        Set<UUID> visited = new HashSet<>();
        java.util.Map<UUID, UUID> predecessors = new HashMap<>();

        queue.add(startId);
        visited.add(startId);
        predecessors.put(startId, null);

        while (!queue.isEmpty()) {
            UUID currentId = queue.poll();
            Planet currentPlanet = planets.get(currentId);

            if (currentPlanet == null || currentPlanet.getResource() == null || currentPlanet.getNeighbours().isEmpty())
                continue;

            if (currentPlanet.getResource().getResourceType().equals(type))
                return constructPath(predecessors, currentId);

            for (java.util.Map.Entry<CompassDirection, UUID> entry: currentPlanet.getNeighbours().entrySet() ) {
                UUID neighbourId = entry.getValue();

                if (!visited.contains(neighbourId)) {
                    queue.add(neighbourId);
                    visited.add(neighbourId);
                    predecessors.put(neighbourId, currentId);
                }
            }
        }
        return new ArrayList<>();
    }

    /**
     * Constructs a path from one UUID to another specified UUID
     * @param predecessors
     * @param targetId
     * @return
     */
    private static List<UUID> constructPath(java.util.Map<UUID, UUID> predecessors, UUID targetId ) {
        LinkedList<UUID> path = new LinkedList<>();

        if (predecessors.size() == 1) return path;

        for (UUID at = targetId; at != null; at = predecessors.get(at)) {
            path.addFirst(at);
        }
        path.removeFirst();
        return path;
    }

    /**
     * Returns the UUID of a neighbouring planet that is unknown or throws an exception if all are known.
     * If the movement difficulty of the planet is higher than the available energy, an exception is thrown
     * @param planets A list of all known planets
     * @param planet The current planet
     * @param energy The available energy for a specific robot
     * @return The UUID of an unknown neighbouring planet
     * @throws MovementException
     */
    public static UUID exploreMap(
            HashMap<UUID, Planet> planets,
            Planet planet,
            Integer energy
    ) throws MovementException {
        if ( planet.getMovementDifficulty() > energy ) throw new MovementException( "You can't move without energy!" );

        for (UUID planetId: planet.getNeighbours().values()) {
            if ( !planets.containsKey( planetId ) ) return planetId;
        }

        throw new MovementException( "All neighbours are known!" );
    }
}
