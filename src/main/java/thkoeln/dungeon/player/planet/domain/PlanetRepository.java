package thkoeln.dungeon.player.planet.domain;

import lombok.Getter;
import org.springframework.stereotype.Repository;

import java.util.*;

@Getter
@Repository
public class PlanetRepository {
    private final HashMap<UUID, Planet> planets = new HashMap<>();

    public Planet findById( UUID id ) {
        return planets.get( id );
    }

    public List<Planet> findAll() {
        return new ArrayList<>( planets.values() );
    }

    public Boolean existsById(UUID id ) {
        return planets.containsKey( id );
    }

    public void save( Planet planet ) {
        if ( planet != null ) {
            planets.put(planet.getPlanetId(), planet);
        }
    }

    public void deleteAll() {
        planets.clear();
    }


}
