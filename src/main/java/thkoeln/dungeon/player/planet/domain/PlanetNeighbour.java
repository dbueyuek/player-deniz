package thkoeln.dungeon.player.planet.domain;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import thkoeln.dungeon.player.core.domainprimitives.location.CompassDirection;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetNeighboursDto;

import java.util.UUID;

@Getter
@ToString
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class PlanetNeighbour {
    private UUID id;
    private CompassDirection direction;

    public PlanetNeighbour(PlanetNeighboursDto planetNeighboursDto) {
        this.id = planetNeighboursDto.getId();
        this.direction = planetNeighboursDto.getDirection();
    }
}
