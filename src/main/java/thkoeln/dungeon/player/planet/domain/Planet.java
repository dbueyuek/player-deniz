package thkoeln.dungeon.player.planet.domain;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import thkoeln.dungeon.player.core.domainprimitives.location.CompassDirection;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

@Entity
@Setter
@Getter
@ToString
@NoArgsConstructor
public class Planet {

    @Id
    private UUID planetId;
    private UUID gameWorldId;
    private Integer movementDifficulty;
    @Embedded
    private PlanetResource resource;
    @ElementCollection(fetch = FetchType.EAGER)
    @CollectionTable(name = "planet_neighbours", joinColumns = @JoinColumn(name = "planet_planetId"))
    @MapKeyEnumerated(EnumType.STRING)
    private Map<CompassDirection, UUID> neighbours = new HashMap<>();

    public Planet(UUID planetId, UUID gameWorldId, Integer movementDifficulty, String resource) {
        this.planetId = planetId;
        this.gameWorldId = gameWorldId;
        this.movementDifficulty = movementDifficulty;
        if (resource != null && !resource.isEmpty()) {
            this.resource = new PlanetResource(resource);
        }
    }
}
