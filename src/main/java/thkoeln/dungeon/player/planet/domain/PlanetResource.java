package thkoeln.dungeon.player.planet.domain;

import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import thkoeln.dungeon.player.core.domainprimitives.location.MineableResourceType;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetResourceDto;

@Getter
@ToString
@Embeddable
@NoArgsConstructor
@AllArgsConstructor
public class PlanetResource {
    private MineableResourceType resourceType;
    private Integer maxAmount;
    private Integer currentAmount;

    public PlanetResource(String resourceType) {
        this.resourceType = MineableResourceType.valueOf(resourceType);
        this.maxAmount= 0;
        this.currentAmount = 0;
    }

    public PlanetResource(PlanetResourceDto planetResourceDto) {
        this.resourceType = planetResourceDto.getResourceType();
        this.maxAmount = planetResourceDto.getMaxAmount();
        this.currentAmount = planetResourceDto.getCurrentAmount();
    }
}
