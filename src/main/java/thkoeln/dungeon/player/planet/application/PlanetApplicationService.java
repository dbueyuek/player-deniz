package thkoeln.dungeon.player.planet.application;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import thkoeln.dungeon.player.core.events.concreteevents.game.GameStatusEvent;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetDiscoveredEvent;
import thkoeln.dungeon.player.core.events.concreteevents.planet.PlanetNeighboursDto;
import thkoeln.dungeon.player.core.events.concreteevents.planet.ResourceMinedEvent;
import thkoeln.dungeon.player.core.events.concreteevents.robot.move.RobotMovePlanetDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotPlanetDto;
import thkoeln.dungeon.player.planet.domain.Planet;
import thkoeln.dungeon.player.planet.domain.PlanetRepository;
import thkoeln.dungeon.player.planet.domain.PlanetResource;
import thkoeln.dungeon.player.player.application.PlayerApplicationService;

import java.util.UUID;

@Service
public class PlanetApplicationService {

    private final PlanetRepository planetRepository;
    private final Logger logger = LoggerFactory.getLogger(PlayerApplicationService.class);

    @Autowired
    public PlanetApplicationService( PlanetRepository planetRepository ) {
        this.planetRepository = planetRepository;
    }

    /**
     * Creates and returns a planet, from a RobotPlanetDto
     * @param planetDto
     * @return A newly created planet
     */
    public Planet newPlanetFromPlanetDto( RobotPlanetDto planetDto ) {
        Planet planet = new Planet(
                planetDto.getPlanetId(),
                planetDto.getGameWorldId(),
                planetDto.getMovementDifficulty(),
                planetDto.getResourceType());
        planetRepository.save( planet );
        return planet;
    }

    /**
     * Creates and return a newly created planet, from a RobotMovePlanetDto and the planet the robots comes from
     * @param planet
     * @param planetDto
     * @return A newly created planet
     */
    public Planet newPlanetFromMovePlanetDto(Planet planet, RobotMovePlanetDto planetDto) {
        Planet newPlanet = new Planet(
                planetDto.getId(),
                planet.getGameWorldId(),
                planetDto.getMovementDifficulty(),
                null);
        planetRepository.save( newPlanet );
        return newPlanet;
    }

    /**
     * Creates and returns a new planet, from a UUID. The planet only contains the ID accordingly
     * @param planetId
     * @return A newly created planet
     */
    public Planet newPlanetFromUUID( UUID planetId ) {
        Planet newPlanet = new Planet(
                planetId,
                null,
                null,
                null);
        planetRepository.save( newPlanet );
        return newPlanet;
    }

    /**
     * Updates infos for a newly discovered planet and saves them
     * @param event
     * @throws Exception
     */
    @EventListener (PlanetDiscoveredEvent.class)
    public void onPlanetDiscovered( PlanetDiscoveredEvent event ) throws Exception {
        if (!event.isValid()) return;
        if ( !planetRepository.existsById( event.getPlanetId() ) )
            throw new Exception("I don't think this should happen.");

        Planet planet = planetRepository.findById(event.getPlanetId());

        if ( event.getResource() != null )
            planet.setResource(new PlanetResource( event.getResource() ));
        if ( event.getMovementDifficulty() != null )
            planet.setMovementDifficulty( event.getMovementDifficulty() );

        for ( PlanetNeighboursDto dto : event.getNeighbours() ) {
            planet.getNeighbours().put(dto.getDirection(), dto.getId());
        }
        planetRepository.save( planet );
        logger.info("Planet discovered: {}", planet);

    }

    /**
     * Updates a planet after it was mined on
     * @param event A ResourceMinedEvent, containing all remaining resources on a planet and the mined amount
     */
    @EventListener(ResourceMinedEvent.class)
    public void onResourceMined( ResourceMinedEvent event ) {
        if ( !planetRepository.existsById( event.getPlanetId() ) ) return;
        if ( !event.isValid() ) return;

        Planet planet = planetRepository.findById(event.getPlanetId());

        planet.setResource(new PlanetResource(
                planet.getResource().getResourceType(),
                event.getResource().getMaxAmount(),
                event.getResource().getCurrentAmount()
                )
        );
        planetRepository.save(planet);
        logger.info( "{} {} was mined from Planet: {} PlanetResource: {}.",
                event.getMinedAmount(), planet.getResource().getResourceType(), planet.getPlanetId(), planet.getResource() );
        logger.info( "Updated PlanetResource: {}.", planet.getResource() );
    }

    /**
     * Deletes all the saved planets, after a game has been finished
     * @param event A GameStatusEvent, containing infos on the current game
     */
    @EventListener( GameStatusEvent.class )
    public void gameStatusEvent( GameStatusEvent event ) {
        if (!event.isValid()) return;
        if (!event.getStatus().isActive())
            planetRepository.deleteAll();
    }
}
