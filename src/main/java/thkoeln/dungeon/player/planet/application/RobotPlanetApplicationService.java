package thkoeln.dungeon.player.planet.application;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import thkoeln.dungeon.player.core.events.concreteevents.robot.move.RobotMovePlanetDto;
import thkoeln.dungeon.player.core.events.concreteevents.robot.spawn.RobotPlanetDto;
import thkoeln.dungeon.player.planet.domain.Planet;
import thkoeln.dungeon.player.planet.domain.PlanetRepository;
import thkoeln.dungeon.player.robot.application.RobotSpecificPlanetApplicationService;

import java.util.HashMap;
import java.util.UUID;

@Service
public class RobotPlanetApplicationService implements RobotSpecificPlanetApplicationService {
    private final PlanetApplicationService planetApplicationService;
    private final PlanetRepository planetRepository;

    @Autowired
    public RobotPlanetApplicationService(
            PlanetApplicationService planetApplicationService,
            PlanetRepository planetRepository
    ) {
        this.planetApplicationService = planetApplicationService;
        this.planetRepository = planetRepository;
    }

    /**
     * Returns a newly created Planet to the RobotApplicationService
     * @param planetDto
     * @return A newly created Planet
     */
    @Override
    public Planet getPlanetFromDto(RobotPlanetDto planetDto) {
        return planetApplicationService.newPlanetFromPlanetDto( planetDto );
    }

    /**
     * Returns a newly created planet to the RobotApplicationService
     * @param oldPlanet
     * @param planetDto
     * @return
     */
    @Override
    public Planet getPlanetFromMoveDto(Planet oldPlanet, RobotMovePlanetDto planetDto) {
        if ( !planetRepository.existsById(planetDto.getId()) )
            return planetApplicationService.newPlanetFromMovePlanetDto(oldPlanet, planetDto);
        else return planetRepository.findById(planetDto.getId());
    }

    /**
     * Returns a newly created planet to the RobotApplicationService
     * @param planetId
     * @return
     */
    @Override
    public Planet getPlanetFromUUID(UUID planetId) {
        if ( !planetRepository.existsById(planetId) )
            return planetApplicationService.newPlanetFromUUID(planetId);
        else return planetRepository.findById(planetId);
    }

    /**
     * Returns all the known planets to the robotApplicationService
     * @return A Hashmap of all known planets
     */
    @Override
    public HashMap<UUID, Planet> getPlanets() {
        return planetRepository.getPlanets();
    }
}
